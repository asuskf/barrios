from rest_framework.views import APIView
from rest_framework.response import Response
from rest.necesito.serializer import NecesitoSerializer
from rest.necesito.models import Necesito
from rest_framework import permissions, viewsets
from django.shortcuts import get_object_or_404

class NecesitoViewSet(viewsets.ModelViewSet):
    queryset = Necesito.objects.all()
    serializer_class = NecesitoSerializer
    #http_method_names = [u'post']
    permission_classes = [permissions.AllowAny]