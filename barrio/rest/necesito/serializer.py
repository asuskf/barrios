from rest_framework import serializers
from rest.necesito.models import Necesito
class NecesitoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Necesito
        fields = ('tipo_necesidad', 'ubicacion_necesidad', 'detalles', 'documento', 'telefono')