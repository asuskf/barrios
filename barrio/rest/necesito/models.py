from django.db import models

class Necesito(models.Model):
    tipo_necesidad = models.CharField(max_length=3)
    ubicacion_necesidad = models.CharField(max_length=256)
    detalles = models.CharField(max_length=256)
    documento = models.CharField(max_length=50,null=True,blank=True)
    telefono = models.CharField(max_length=15,null=True,blank=True)