from django.urls import include, path
from rest_framework import routers
from rest.necesito import views

app_name = 'security'

router = routers.DefaultRouter()
router.register(r'necesitoEndpoint', views.NecesitoViewSet)

urlpatterns = [
    path(r'', include(router.urls), name='base')
]