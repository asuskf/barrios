#!/usr/bin/env bash

docker build --no-cache -t registry.gitlab.com/wikicarlos/barrios/barrios:latest .
docker push registry.gitlab.com/wikicarlos/barrios/barrios:latest
